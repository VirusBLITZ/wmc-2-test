import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Card } from '../card/card.component';

@Component({
  selector: 'app-card-maker',
  templateUrl: './card-maker.component.html',
  styleUrls: ['./card-maker.component.css']
})
export class CardMakerComponent {
  @Input() copied!: Card;
  @Output() create = new EventEmitter<Card>();

  createCard() {
    this.create.emit({ ...this.copied })
    this.copied.title = '';
  }
}
