import { Component } from '@angular/core';
import { Card } from './card/card.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cards: Card[] = [
    {
      title: 'Card 1'
    },
    {
      title: 'Card 2'
    },
    {
      title: 'Card 3'
    },
    {

      title: 'Card 4'
    },
  ]

  template: Card = {
    title: ''
  };

  deleteCard(card: Card) {
    const idx = this.cards.indexOf(card);
    this.cards.splice(idx, 1);
    // this.cards = this.cards.filter(card => card.id !== $event.id);
  }

  createCard(card: Card) {
    this.cards.push(card);
  }

  copyCard($event: Card) {
    this.template = { ...$event };
  }
}
