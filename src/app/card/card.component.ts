import { Component, EventEmitter, Input, Output } from '@angular/core';

export interface Card {
  title: string;
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input({required: true}) inCard!: Card;
  @Output() delete = new EventEmitter<Card>();
  @Output() copy = new EventEmitter<Card>();

  deleteCard() {
    this.delete.emit(this.inCard);
  }

  copyCard() {
    this.copy.emit(this.inCard);
  }
}
